//
//  AppDelegate.h
//  WebServiceHandler
//
//  Created by HelixTech-Admin on 30/11/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

